'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var browserSync = require('browser-sync').create();
var pxtorem = require('gulp-pxtorem');




gulp.task('sass', function () {
	return gulp.src('wwwroot/front/resources/scss/**/*.scss')
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 4 versions'],
			cascade: true
		}))
		.pipe(pxtorem({
			propList: ['*', '!'],
			rootValue: 16,
			replace: false,
			minPixelValue: 2,
			mediaQuery: true
		}))
		.pipe(csso({
			comments: 'exclamation'
		}))
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest('wwwroot/front/resources/styles'))
		.pipe(browserSync.stream());
		//.on('end', autoPrefixer);
});

gulp.task('watch', function () {

    browserSync.init({
        //logLevel: "debug",
        port : 3333,
        open: false,
        directory: true,
        server: "./wwwroot/",
        browser: "google chrome"
    });


    //gulp.watch('src/**/*.js',  gulp.series('uglify'));
    //gulp.watch('src/**/*.scss', gulp.series('minifycss'));
    gulp.watch('wwwroot/front/resources/scss/**/*.scss', gulp.series('sass'));
    gulp.watch("wwwroot/**/*.html").on('change', browserSync.reload);
});

gulp.task('default', gulp.series('sass', 'watch')); 
