(function() {
	function init() {
		getCurrentPage();
		getProgress();
		setRange();
		setPrice();
		setPagination();
	}

	function getProgress() {
		var _valid = 0,
			_requireds = document.querySelectorAll('[required]'),
			_inputs = document.querySelectorAll('.main [name]'),
			_progress = document.querySelector('.progress-chart-bar span'),
			_current = document.querySelector('.pagination--current');


		[].forEach.call(_inputs, function(_form) {
			_form.removeEventListener('change', getProgress);
			_form.addEventListener('change', getProgress);
		});

		[].forEach.call(_requireds, function(_form) {
			_form.removeEventListener('change', getProgress);
			_form.addEventListener('change', getProgress);

			if(_form.validity.valid === true) {
				_valid++;
			}
		});

		_current.innerHTML = _valid;

		_progress.style.width = (_valid / _requireds.length * 100) + '%';
	}

	function getCurrentPage() {
		var _sections = document.querySelectorAll('section.diagnose'),
			_page = (location.hash.replace('#', '') || '/').split('/'),
			_currentIndex = 0,
			_session = sessionStorage.getItem('diagnose');

		if(_session === null) {
			if(history.pushState) {
				history.pushState('', '/', window.location.pathname);
			} else {
				location.hash = '';
			}
			return;
		}

		_session = JSON.parse(_session);

		for(var i in _session) {
			var _form = document.querySelector('[name="'+ i +'"]');
			if(_form.getAttribute('type') === 'radio') {
				[].forEach.call(document.querySelectorAll('[name="'+ i +'"]'), function(_radio) {
					if(_radio.value == _session[i]) {
						_radio.checked = true;
					}
				});
			} else {
				_form.value = _session[i];
				_form.dispatchEvent(new Event('change'));
			}
		}

		_page = _page[2] ? _page[2] : _page[1];

		_currentIndex = _page.length > 0 ? [].findIndex.call(_sections, function(_section, _index) {
			return _section.dataset.page == _page;
		}) : 0;

		[].forEach.call(_sections, function(_section, _index) {
			if(_currentIndex > _index) {
				_section.dataset.step = 'prev';
			} else if(_currentIndex < _index) {
				_section.dataset.step = 'next';
			} else {
				_section.dataset.step = 'current';
			}
		});
	}

	function setCurrentPage(_section) {
		var _hash = _section.dataset.section ==  _section.dataset.page ?
			_section.dataset.page :
			_section.dataset.section + '/' + _section.dataset.page,
			_session = sessionStorage.getItem('diagnose');

		document.documentElement.dataset.section = _section.dataset.section;
		document.documentElement.dataset.page = _section.dataset.page;

		if(_session) {
			history.pushState('', '', '#/' + _hash);
		}
	}

	function setPagination() {
		var _btnPrevs = document.querySelectorAll('button[data-button-step="prev"]'),
			_btnNexts = document.querySelectorAll('button[data-button-step="next"]');

		[].forEach.call(_btnPrevs, function(_btn) {
			_btn.addEventListener('click', function(e) {
				var _section = this.closest('section');

				_section.dataset.step = 'next';
				_section.previousElementSibling.dataset.step = 'current';

				setCurrentPage(_section.previousElementSibling);
			});
		});

		[].forEach.call(_btnNexts, function(_btn) {
			_btn.addEventListener('click', function(e) {
				var _section = this.closest('section'),
					_errorCount = 0;

				[].forEach.call(_section.querySelectorAll('[required]'), function(_form) {
					var _parent = _form.closest('.question__input');

					_parent.classList.remove('error');
					if(_form.validity.valid === false) {
						_parent.classList.add('error');
						_errorCount++;
					}
				});

				if(_errorCount > 0) {
					return false;
				}

				_section.dataset.step = 'prev';
				_section.nextElementSibling.dataset.step = 'current';

				//console.log( $('#frm').serializeArray() );
				console.log(document.querySelector('#frm').serialize());

				sessionStorage.setItem('diagnose', document.querySelector('#frm').serialize());

				setCurrentPage(_section.nextElementSibling);
			});
		});
	}

	function setRange() {
		var _ranges = document.querySelectorAll('.question-range');

		[].forEach.call(_ranges, function(_range) {
			_range.addEventListener('change', function() {
				var _target = document.querySelector('input[name="'+ this.dataset.target +'"]');

				_target.value = parseFloat(this.value);
				_target.dispatchEvent(new Event('change'));
			});
		});
	}

	function setPrice() {
		var _price = document.querySelectorAll('.price-input');

		if(_price.length < 0) {
			return false;
		}

		[].forEach.call(_price, function(_wrap) {
			_wrap.querySelector('input[type="number"]').addEventListener('change', function() {
				var _numberText = this.closest('.question__input').querySelector('.number-text');
				this.nextElementSibling.innerHTML = this.value.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
				if(_numberText) {
					_numberText.innerHTML = viewKorean(this.value);
				}
			});
		});
	}

	function viewKorean(num) {
		var hanA = new Array('', '일', '이', '삼', '사', '오', '육', '칠', '팔', '구', '십'),
			danA = new Array('', '십', '백', '천', '', '십', '백', '천', '', '십', '백', '천', '', '십', '백', '천'),
			result = '';

		for(i = 0; i < num.length; i++) {
			str = '';
			han = hanA[num.charAt(num.length - (i + 1))];
			if(han != '') str += han + danA[i];
			if(i == 4) str += '만';
			if(i == 8) str += '억';
			if(i == 12) str += '조';
			result = str + result;
		}

		if(num != 0) result = result + '원';

		return result;
	}

	document.addEventListener('DOMContentLoaded', init);
})();