if(window.Element && !Element.prototype.closest) {
	Element.prototype.closest =
	function(s) {
		var matches = (this.document || this.ownerDocument).querySelectorAll(s),
			i,
			el = this;
		do {
			i = matches.length;
			while(--i >= 0 && matches.item(i) !== el) {};
		} while((i < 0) && (el = el.parentElement));
		return el;
	};
}

if(!Array.prototype.findIndex) {
	Object.defineProperty(Array.prototype, 'findIndex', {
		value: function(predicate) {
			'use strict';
			if (this == null) {
				throw new TypeError('Array.prototype.findIndex called on null or undefined');
			}
			if (typeof predicate !== 'function') {
				throw new TypeError('predicate must be a function');
			}
			var list = Object(this);
			var length = list.length >>> 0;
			var thisArg = arguments[1];
			var value;

			for (var i = 0; i < length; i++) {
				value = list[i];
				if (predicate.call(thisArg, value, i, list)) {
				return i;
				}
			}
			return -1;
		},
		enumerable: false,
		configurable: false,
		writable: false
	});
}

HTMLElement.prototype.serialize = function(){
	var obj = {};
	var elements = this.querySelectorAll('input, select, textarea');

	for(var i = 0; i < elements.length; ++i) {
		var element = elements[i];
		var name = element.name;
		var value = element.value;

		if(name && element.validity.valid === true) {
			if(element.type === 'radio') {
				if(element.checked) {
					obj[name] = value;
				}
			} else {
				obj[name] = value;
			}
		}
	}
	return JSON.stringify(obj);
	//return obj;
};


(function(window, document) {
	var docEl = document.documentElement,
		baseFontPercent = 100;


	var setFontSize = function() {
		var fontSizeVal = parseFloat((docEl.offsetWidth / 480) * baseFontPercent); //docEl.clientWidth

		fontSizeVal = (fontSizeVal >= 100) ? 100 : fontSizeVal;
		docEl.style.fontSize = fontSizeVal + '%';
	};

	setFontSize();
	window.addEventListener('resize', setFontSize);
})(window, document);

(function() {
	var positionStickySupport = function() {var el=document.createElement('a'),mStyle=el.style;mStyle.cssText="position:sticky;position:-webkit-sticky;position:-ms-sticky;";return mStyle.position.indexOf('sticky')!==-1;}();

	function progressSticky() {
		var _progress = document.querySelector('.progress'),
			_stickyWrap = document.createElement('div');

		Element.prototype.wrap = function(wrapper) {
			this.parentNode.insertBefore(wrapper, this);
			wrapper.appendChild(this);
		};

		if(positionStickySupport) {
			return false;
		}

		_progress.wrap(_stickyWrap);
		_stickyWrap.classList.add('progress-wrap');
		_stickyWrap.style.height = _progress.offsetHeight + 'px';

		window.addEventListener('scroll', function() {
			if(this.pageYOffset >= _stickyWrap.offsetTop) {
				_progress.classList.add('js-sticky');
			} else {
				_progress.classList.remove('js-sticky');
			}
		});
	};


	document.addEventListener('DOMContentLoaded', progressSticky);
	document.addEventListener('DOMContentLoaded', function() {
		[].forEach.call(document.querySelectorAll('.question-arrow'), function(arrow) {
			if (arrow.querySelector('a')) {
				arrow.querySelector('a').addEventListener('click', function (e) {
					var _parent = this.parentNode;
					_parent.classList.toggle('active');

					if (_parent.classList.contains('active')) {
						document.addEventListener('click', function (e) {
							if (e.target.parentNode == _parent) {
							} else {
								_parent.classList.remove('active');
							}
						});
					}

					e.preventDefault();
					return false;
				});
			}
		});

		function selectTabs() {
			var _tabBtns = this.closest('div').querySelectorAll('span');
			
			var _thisTab = this.closest('.intro-video');
			[].forEach.call(_thisTab.querySelectorAll('.intro-video__contents .video-content'), function (_tab, _key) {
				_tab.classList.add('hidden');
				_tabBtns[_key].classList.remove('active');
			});

			this.parentNode.classList.add('active');
			document.querySelector(this.dataset.target).classList.remove('hidden');
		}
		[].forEach.call(document.querySelectorAll('.intro-video__tabs button'), function(_tab) {
			_tab.addEventListener('click', selectTabs);
		});
	});
})();