const COUNT_WORD = '명';
const KAKAO_APP_KEY = 'fce577632b1fe4b5bd1e3c0c467a95f4';
const REGEX_EMAIL = /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/;
let requestPath = document.location.pathname;
let fstPath = requestPath.substr(0, requestPath.lastIndexOf('/'));

let contents = [];
let prevUrl = document.referrer;
let offset = prevUrl.lastIndexOf('/');
let prevUri = prevUrl.substring(offset + 1, prevUrl.length);

let popup = $('#popup');
let error = $('#popup-error');
let more = $('#prescription-more');
let content = $('.diagnose');
let questionInput = $('.question-input');
let q4 = $('#q4');
let spanFamilyNumber = $('.family-number');
let btnLayerClose = $('.layer .btn');
let btnStart = $('.btn-start');
let btnEnd = $('#end');
let btnFinish = $('#finish');
let btnDetailPrescription = $('#detail-prescription');
let info = $('#info');
let navTab = $('.nav-tab');
let popupUrl = $('#popup-url');
let popupMail = $('#popup-mail');
let popupMailComplete = $('#popup-mail-completed');
let isA1PopupShown = false;

$(document).ready(function () {
	if (info.length) {
		info.hide();
	}

	if (popup.length) {
		popup.hide();
	}

	if (error.length) {
		error.hide();
	}

	if (more.length) {
		more.hide();
	}

	if (popupMail.length) {
		popupMail.hide();
	}

	if (popupMailComplete.length) {
		popupMailComplete.hide();
	}

	let count = countAnswer();
	if (!(fstPath === '/results' || fstPath === '/prescription')) {
		$('.pagination--current').text(count);
		$('#questionCount').text(28 - count);
	}
	progress(count);

	$('.share-kakao').on('click', function (e) {
		e.preventDefault();
		let mainUrl = getHostname();
		let resultUrl = document.location.href;
		let _start = requestPath.indexOf('/') + 1;
		let _end = requestPath.lastIndexOf('/') - 1;
		if (requestPath.substr(_start, _end) === 'prescription') {
			resultUrl = resultUrl.replace('prescription', 'results');
		}
		let imageUrl = mainUrl + '/resources/images/share.png';
		sendTalk(mainUrl, resultUrl, imageUrl);
	});

	$('.share-fb').on('click', function (e) {
		e.preventDefault();
		let resultUrl = encodeURIComponent(window.location.href);
		let _start = requestPath.indexOf('/') + 1;
		let _end = requestPath.lastIndexOf('/') - 1;
		if (requestPath.substr(_start, _end) === 'prescription') {
			resultUrl = resultUrl.replace('prescription', 'results');
		}
		let title = encodeURIComponent('재무건강진단');
		openPopup("https://www.facebook.com/sharer/sharer.php?u=" + resultUrl + "&t=" + title, 555, 330);
	});

	$('.share-mail').on('click', function (e) {
		e.preventDefault();
		popupMail.show();
		popupMail.css('z-index', 2);
	});

	$('.share-print, .btn-print').on('click', function (e) {
		e.preventDefault();
		window.print();
	});

	$('.share-url').on('click', function (e) {
		e.preventDefault();
		let dummy = document.createElement('input'),
			text = window.location.href;

		let _start = requestPath.indexOf('/') + 1;
		let _end = requestPath.lastIndexOf('/') - 1;
		if (requestPath.substr(_start, _end) === 'prescription') {
			text = text.replace('prescription', 'results');
		}

		document.body.appendChild(dummy);
		dummy.value = text;
		dummy.select();
		document.execCommand('copy');
		document.body.removeChild(dummy);

		popupUrl.show();
		popupUrl.css('z-index', 2);
	});

	$('.icon-arrow-down').on('click', function () {
		$('.progress-all').toggle();
		if ($('.icon-arrow-down').hasClass('active')) {
			$('.icon-arrow-down').removeClass('active');
		} else {
			$('.icon-arrow-down').addClass('active');
		}
	});

	$('.a-category-large').on('click', function () {
		$('.category-main').each(function () {
			$(this).removeClass('active');
		});
		$('.menu-sub').each(function () {
			$(this).removeClass('active');
		});
		$(this).parent().addClass('active');
		if ($(window).width() < 768) {
			$('.select-category--wrap').removeClass('active');
		}
	});

	$('.a-category-middle').on('click', function () {
		$('.category-main').each(function () {
			$(this).removeClass('active');
		});
		$('.menu-sub').each(function () {
			$(this).removeClass('active');
		});
		let refId = $(this).parent().data('middle-ref-id');
		$('li[data-large-id=' + refId + ']').addClass('active');
		$(this).parent().addClass('active');
		if ($(window).width() < 768) {
			$('.select-category--wrap').removeClass('active');
		}
	});

	$('.btn-toggle').on('click', function (e) {
		e.preventDefault();
		let isActive = false;
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			isActive = false;
		} else {
			$(this).addClass('active');
			isActive = true;
		}
		let next = $(this).next();
		while (next.length) {
			if (isActive) {
				next.show();
			} else {
				next.hide();
			}
			next = next.next();
		}
	});

	$('.icon-info').on('click', function (e) {
		e.preventDefault();
		info.show();
		info.css('z-index', 2);
		info.focus();
		$("html, body").css({overflow:"hidden"});
	});

	$('.btn-close, .btn-bottom-close').on('click', function (e) {
		e.preventDefault();
		$(this).closest('.main').hide();
		$("html, body").css({overflow:"visible"});

		e.stopImmediatePropagation();
	});

	$('.btn-offset-top').on('click', function (e) {
		e.preventDefault();
		$(this).closest('.main').animate({ scrollTop: 0 }, 500);

		e.stopImmediatePropagation();
	});

	$('#btn-go-exam').on('click', function (e) {
		if (validation(undefined, undefined, validationCallback)) {
			e.preventDefault();
			$('.diagnose-waiting').hide();
			$('.diagnose-start').show();
			$('html, body').animate({ scrollTop: 0 }, 500);
		}
	});

	q4.on('change', function () {
		if ($(this).val() > 1) {
			spanFamilyNumber.text($(this).val() + COUNT_WORD);
			popup.show();
			popup.css('z-index', 2);
		}
	});

	btnStart.on('click', function (e) {
		e.preventDefault();
		popup.show();
		popup.css('z-index', 2);
	});

	btnEnd.on('click', function (e) {
		e.preventDefault();
		popup.show();
		popup.css('z-index', 2);
	});

	btnFinish.on('click', function (e) {
		e.preventDefault();
		popup.show();
		popup.css('z-index', 2);
	});

	btnDetailPrescription.on('click', function (e) {
		e.preventDefault();
		more.toggle();
		more.css('z-index', 2);
		if ($(window).width() < 768) {
			more.css('background-color', 'inherit');
		}
		if (!$('.select-category--wrap').hasClass('active')) {
			$('.select-category--wrap').addClass('active');
		}
		$("html, body").css({overflow:"hidden"});
	});

	questionInput.each(function () {
		if ($(this).attr('type') === 'email') {
			return;
		}
		$(this).maskMoney({
			thousands: ',',
			allowEmpty: true,
			allowZero: true,
			precision: '0'
		});
	});

	questionInput.on('focus', function (e) {
		e.preventDefault();
		if ($(this).closest('.content').hasClass('exam-income')) {
			if (isA1PopupShown) {
				if (!$(this).val()) {
					$(this).val(0);
				}
				return;
			} else {
				spanFamilyNumber.text(q4.val() + COUNT_WORD);
				popup.show();
				popup.css('z-index', 2);
				isA1PopupShown = true;
			}
		}
		if ($(this).attr('type') !== 'email' && !$(this).val()) {
			$(this).val(0);
		}
	});

	$('[type=range]').each(function () {
		let val = $('#' + $(this).attr('id').replace('_range', '')).val();
		if (!val) {
			$(this).val($(this).attr('min'));
		}
	});

	$("input").on('change', function () {
		let count = countAnswer();
		$('.pagination--current').text(count);
		$('#questionCount').text(28 - count);
		progress(count);
	});

	$("select").on('change', function () {
		let count = countAnswer();
		$('.pagination--current').text(count);
		$('#questionCount').text(28 - count);
		progress(count);
	});

	if (fstPath === '/results') {
		$('.btn-toggle').each(function () {
			let next = $(this).next();
			while (next.length) {
				next.hide();
				next = next.next();
			}
		});
	}

	if (btnLayerClose) {
		btnLayerClose.click(function (e) {
			e.preventDefault();
			if (popup.length) {
				popup.hide();
			}
			if (error.length) {
				error.hide();
			}
			if (popupUrl.length) {
				popupUrl.hide();
			}
			if (popupMailComplete.length) {
				popupMailComplete.hide();
			}

			if ($(this).closest('.layer').attr('id') === 'popup-error') {
				return;
			}

			if (requestPath === '/' || requestPath === '' || requestPath === '/intro') {
				document.location = '/survey/Q4';
			} else if (requestPath === '/survey/Q4') {
				// nothing to do
			} else if (requestPath === '/survey/A8') {
				$('.exam-income .question-input').focus();
			} else if (requestPath === '/survey/B7') {
				$('html, body').animate({ scrollTop: 0 }, 500);
			} else if (requestPath === '/survey/C9') {
				document.form.submit();
			} else if (fstPath === '/results') {
				let uid = requestPath.substr(requestPath.lastIndexOf('/') + 1, requestPath.length);
				document.location = '/prescription/' + uid;
			} else if (fstPath === '/prescription') {
				if (popupMail.is(':visible')) {
					let email = $("input[type='email']").val();
					let uid = requestPath.substr(requestPath.lastIndexOf('/') + 1, requestPath.length);
					mail.data = {
						email: email,
						uid: uid
					};
					if (REGEX_EMAIL.test(email)) {
						if (mail.isRun) {
							alert('메일 전송중입니다.');
							return;
						}
						mail.send();
					} else {
						alert("메일 주소가 올바르지 않습니다.");
					}
				}
			} else {
				// nothing to do
				$('html, body').animate({ scrollTop: 0 }, 500);
			}
		});
	}

	if (navTab) {
		if (requestPath === '/about') {
			$(navTab[0]).removeClass('active');
			$(navTab[1]).addClass('active');
		} else {
			$(navTab[0]).addClass('active');
			$(navTab[1]).removeClass('active');
		}
	}

	if (questionInput) {
		questionInput.each(function () {
			$(this).css('color', '#797979');
			if ($(this).val() > 0) {
				displayAmountToKoreanWord($(this), $(this).attr('name') + '_txt');
			}
		});
	}

	if (requestPath === '/survey/A8' && content) {
		content.each(function () {
			let section = $(this);

			let classes = section.attr('class').split(/\s+/);
			if (!classes) {
				return;
			}

			let clazz = classes[classes.length - 1];

			if (clazz === 'alert') {
				return;
			}

			section.hide();

			let prev = contents.length - 1;
			contents.push(clazz);
			let next = contents.length;

			if (prevUri === 'B7' && clazz === 'exam-debt') {
				section.show();
			} else if (prevUri !== 'B7' && clazz === 'exam-income') {
				section.show();
			} else {
				section.hide();
			}

			section.find('.btn-left').click(function (e) {
				e.preventDefault();
				if (prev < 0) {
					document.form.action = '/survey/Q4';
					document.form.submit();
					return;
				}
				section.hide();
				let left = $('.' + contents[prev]);
				left.show();
				left.find('.question__contents').show();
				$('html, body').animate({ scrollTop: 0 }, 500);
			});

			section.find('.btn-right').click(function (e) {
				e.preventDefault();
				if (validation(section.find('.question-input').attr('name'), next, function (invalidField, next) {
					error.show();
					error.css('z-index', 2);
					let text = $('#' + invalidField).closest('.question__input').closest('.content').find('.question__title').text();
					if (next === 1) {
						text += ' 재무건강의 측정은 기본적 소득이 있는 집을 대상으로 이루어집니다. '
								+ '소득이 없으시다면 우리집 전체의 재무건강을 확인해보세요';
					} else if (next === 5) {
						text += '생활비는 식료품비,의류비,주거비,교통비,통신비 등 일상생활에 쓰이는 모든 돈을 포함합니다. '
								+ '값을 대략적으로 입력해주세요';
					}
					$('#error-message').text(text);
				})) {
					let val = numeral(section.find('.question-input').val()).value();
					section.find('.question-input').val(val);
					section.hide();
					$('.' + contents[next]).show();
					$('html, body').animate({ scrollTop: 0 }, 500);
				} else {
					return;
				}

				if (next === contents.length) {
					document.form.action = '/survey/B7';
					document.form.submit();
				}
			})
		});
	}

	$(document).on("ajaxSend", function (e, xhr, opts) {
		xhr.setRequestHeader("AJAX", true);
		xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
	});

	$('form').submit(function () {
		if (!validation(undefined, undefined, validationCallback)) {
			return false;
		}
	});
});

function validationCallback(invalidField) {
	// event.preventDefault();
	error.show();
	error.css('z-index', 2);
	let index = 0;
	let message = undefined;
	let title = $('#' + invalidField).closest('.question__input').closest('.content').find('.question__title');
	if (title.length > 1) {
		let regex = /^(q|b|c)([0-9]*)/g;
		let match = regex.exec(invalidField);
		if (match.length > 1) {
			index = match[2];
			if (index >= 0) {
				message = $(title[index - 1]).text();
				let offset = $(title[index - 1]).offset().top;
				$('html, body').animate({ scrollTop: offset - 93 }, 500);
			}
		}
	} else {
		message = title.text();
	}
	$('#error-message').text(message);
}

function validation(checkField, next, negativeCallback) {
	let invalidField = undefined;
	if (requestPath === '/survey/Q4') {
		if (!($('#q1').val() >= 1908 && $('#q1').val() <= 1998)) {
			invalidField = 'q1';
		} else if (!($("input[type='radio'][name=q2]").is(':checked'))) {
			invalidField = 'q2';
		} else if (!($("input[type='radio'][name=q3]").is(':checked'))) {
			invalidField = 'q3';
		} else if (!($('#q4').val() >= 1 && $('#q4').val() <= 20)) {
			invalidField = 'q4';
		}
	} else if (requestPath === '/survey/A8') {
		if (!(numeral($('#a1').val()).value() >= 1)) {
			invalidField = 'a1';
		} else if (!(numeral($('#a5').val()).value() >= 0)) {
			invalidField = 'a5';
		} else if (!(numeral($('#a6').val()).value() >= 0 && numeral($('#a6').val()).value() <= 100)) {
			invalidField = 'a6';
		} else if (!(numeral($('#a4').val()).value() >= 0)) {
			invalidField = 'a4';
		} else if (!(numeral($('#a2').val()).value() >= 1)) {
			invalidField = 'a2';
		} else if (!(numeral($('#a3').val()).value() >= 0 && numeral($('#a3').val()).value() <= 60)) {
			invalidField = 'a3';
		} else if (!(numeral($('#a7').val()).value() >= 0)) {
			invalidField = 'a7';
		} else if (!(numeral($('#a8').val()).value() >= 0)) {
			invalidField = 'a8';
		}
	} else if (requestPath === '/survey/B7') {
		for (let i = 1; i <= 7; i++) {
			let name = 'b' + i;
			if (!($("input[type='radio'][name=\'" + name + "\']").is(':checked'))) {
				invalidField = name;
				break;
			}
		}
	} else if (requestPath === '/survey/C9') {
		for (let i = 1; i <= 9; i++) {
			let name = 'c' + i;
			if (!($("input[type='radio'][name=\'" + name + "\']").is(':checked'))) {
				invalidField = name;
				break;
			}
		}
	}
	if ((!checkField && invalidField && negativeCallback) ||
		(checkField && checkField === invalidField)) {
		negativeCallback(invalidField, next);
		return false;
	}

	return true;
}

function showVal(id, val) {
	let obj = $('#' + id);
	if (val > 1000) {
		val = Math.round(val / 100) * 100;
	} else if (val > 100) {
		val = Math.round(val / 10) * 10;
	}
	obj.css('color', '#797979');
	if (val > 999) {
		obj.maskMoney('mask', val);
	} else {
		obj.val(val);
	}
}

function syncRange(obj, id) {
	let value = $(obj).val();
	if (value) {
		$('#' + id).val(numeral(value).value());
	} else {
		$('#' + id).val($('#' + id).attr('min'));
	}
}

function displayAmountToKoreanWord(obj, id) {
	if (!document.getElementById(id)) {
		return;
	}
	document.getElementById(id).innerHTML = amountToKorean("" + numeral($(obj).val()).value() * 10000);
}

function amountToKorean(num) {
	let hanA = ["", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구", "십"];
	let danA = ["", "십", "백", "천", "", "십", "백", "천", "", "십", "백", "천", "", "십", "백", "천"];
	let result = "";
	let str;
	let han;
	for (let i = 0; i < num.length; i++) {
		str = "";
		han = hanA[num.charAt(num.length - (i + 1))];
		if (han !== "") {
			str += han + danA[i];
		}
		if (i === 4) str += "만";
		if (i === 8) str += "억";
		if (i === 12) str += "조";
		result = str + result;
	}
	if (num !== 0) {
		result = result + "원";
	}
	if (result.indexOf('억만원') > 0) {
		result = result.replace('억만원', '억원');
	}
	return result;
}

function openPopup(url, width, height) {
	let popupX = (window.screen.width / 2) - (200 / 2);
	let popupY = (window.screen.height / 2) - (300 / 2);
	window.open(url, "_blank", "top=" + popupY + ",left=" + popupX + ",width=" + width + ",height=" + height + ",toolbar=0,status=0,scrollbars=1,resizable=0");
}

// // 사용할 앱의 JavaScript 키를 설정해 주세요.
//Kakao.init(KAKAO_APP_KEY);

// // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
function sendTalk(mainUrl, resultUrl, imageUrl) {
	Kakao.Link.sendDefault({
		objectType: 'feed',
		content: {
			title: '재무건강 5분 체크인',
			description: '#재무건강검진 #재무건강튼튼 #Financial Health #MetLife Foundation',
			imageUrl: imageUrl,
			link: {
				mobileWebUrl: resultUrl,
				webUrl: resultUrl
			}
		},
		social: {
			likeCount: 286,
			commentCount: 45,
			sharedCount: 845
		},
		buttons: [
			{
				title: '검진결과 확인',
				link: {
					mobileWebUrl: resultUrl,
					webUrl: resultUrl
				}
			},
			{
				title: '진단시작하기',
				link: {
					mobileWebUrl: mainUrl,
					webUrl: mainUrl
				}
			}
		]
	});
}

function progress(count) {
	let start = 0;
	let end = 1;
	let minPercent = 0;
	let maxPercent = 0;
	if (requestPath === '/' || requestPath === '' || requestPath === '/intro') {
		// nothing to do
	} else if (requestPath === '/survey/Q4') {
		start = 0;
		end = 4;
		minPercent = 0;
		maxPercent = 15;
	} else if (requestPath === '/survey/A8') {
		start = 4;
		end = 12;
		minPercent = 15;
		maxPercent = 30;
	} else if (requestPath === '/survey/B7') {
		start = 12;
		end = 19;
		minPercent = 30;
		maxPercent = 43;
	} else if (requestPath === '/survey/C9') {
		start = 19;
		end = 28;
		minPercent = 43;
		maxPercent = 58;
	} else if (fstPath === '/results') {
		$('.progress-summary .progress-chart-bar').children(0).css('width', 80 + '%');
		$('.progress-all .progress-chart-bar').children(0).css('height', 80 + '%')
		return;
	} else if (fstPath === '/prescription') {
		$('.progress-summary .progress-chart-bar').children(0).css('width', 100 + '%');
		$('.progress-all .progress-chart-bar').children(0).css('height', 100 + '%')
		return;
	}
	let percent = minPercent + (((count - start) / (end - start)) * ((maxPercent - minPercent) / 100) * 100);
	$('.progress-summary .progress-chart-bar').children(0).css('width', percent + '%');
	$('.progress-all .progress-chart-bar').children(0).css('height', percent + 5 + '%')
}

function countAnswer() {
	let count = 0;
	for (let i = 1; i <= 4; i++) {
		let obj = $('#q' + i);
		let type = obj.attr('type');
		switch (type) {
			case 'radio':
				if ($("input[type='radio'][name=\'q" + i + "\']").is(':checked')) {
					count++;
				}
				break;
			default:
				if (obj.val()) {
					count++;
				}
				break;
		}
	}

	for (let i = 1; i <= 8; i++) {
		let obj = $('#a' + i);
		let type = obj.attr('type');
		switch (type) {
			case 'text':
			case 'hidden':
			default:
				if (obj.val()) {
					count++;
				}
				break;
		}
	}

	for (let i = 1; i <= 7; i++) {
		let obj = $('#b' + i);
		let type = obj.attr('type');
		switch (type) {
			case 'radio':
				if ($("input[type='radio'][name=\'b" + i + "\']").is(':checked')) {
					count++;
				}
				break;
			case 'hidden':
				if (obj.val()) {
					count++;
				}
				break;
			default:
				break;
		}
	}

	for (let i = 1; i <= 9; i++) {
		let obj = $('#c' + i);
		let type = obj.attr('type');
		switch (type) {
			case 'radio':
				if ($("input[type='radio'][name=\'c" + i + "\']").is(':checked')) {
					count++;
				}
				break;
			case 'hidden':
				if (obj.val()) {
					count++;
				}
				break;
			default:
				break;
		}
	}

	return count;
}

function getHostname() {
	let url = document.location.href;
	let m = url.match(/^https?:\/\/[^/]+/);
	return m ? m[0] : null;
}

let mail = {
	isRun: false,
	data: {},
	send: function () {
		mail.isRun = true;
		$.ajax({
			type: 'POST',
			url: '/api/send/mail',
			data: mail.data,
			dataType: 'json'
		})
			.done(function (data) {
				let code = data['code'];
				let message = data['message'];
				if (code === 200) {
					popupMail.hide();
					popupMailComplete.show();
					popupMailComplete.css('z-index', 2);
				} else {
					console.log(message);
					alert("메일 전송에 실패했습니다. \n잠시후 다시 시도해 주시기 바랍니다.");
				}
			})
			.fail(function (data) {
				console.log(data);
				alert("메일 전송에 실패했습니다. \n잠시후 다시 시도해 주시기 바랍니다.");
			})
			.always(function () {
				mail.isRun = false;
			});
	}
};